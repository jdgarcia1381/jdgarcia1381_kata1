# -*- coding: utf-8 -*-
from unittest import TestCase

from src.calculadora import Calculadora


class TestCalculadora(TestCase):
    def test_calculadora(self):
        self.assertEquals(Calculadora().sumar(""), 0, "Cadena vacía")
        self.assertEquals(Calculadora().sumar("1"), 1, "Un número")
        self.assertEquals(Calculadora().sumar("1,2"), 3, "Dos números")
        self.assertEquals(Calculadora().sumar("1,2,3"), 6, "Tres números")
        self.assertEquals(Calculadora().sumar("1,2,3,4"), 10, "Cuatro números")
        self.assertEquals(Calculadora().sumar("1;2;3;4"), 10, "Cuatro números con separador ;")
        self.assertEquals(Calculadora().sumar("1&2&3&4"), 10, "Cuatro números con separador &")
        self.assertEquals(Calculadora().sumar("1%2%3%4"), 10, "Cuatro números con separador %")
