class Calculadora:
    def sumar(self, cadena):
        cadena = str(cadena)
        cadena = cadena.replace(";", ",")
        cadena = cadena.replace("&", ",")
        cadena = cadena.replace("%", ",")
        suma = 0
        if len(cadena) > 0:
            if len(cadena) == 1:
                suma = int(cadena)
            else:
                cadena_split = cadena.split(",")
                for numero in cadena_split:
                    numero = int(numero)
                    suma += numero
        return suma
